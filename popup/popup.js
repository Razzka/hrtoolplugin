// TODO: поменять хост на препродовый после мержа и на продовый после релиза
const HOST = 'http://feat-spc-1572-chrome-hr-plugin.test.csssr.space';
// const HOST = 'http://localhost:3030';

const SHEETS_MAP = {
  1: '0',
  2: '229602841',
  3: '426199477'
}

let activeTab = {};

async function sendData() {
  const fio = document.getElementById('hr-tool-name-input').value || '';
  const select = document.getElementById('hr-tool-stack-selectlist')
  const source = Array(...select.options).reduce((acc, option) => {
    if (option.selected === true) {
      acc.push(option.value);
    }
    
    return acc;
  }, [])

  chrome.storage.sync.get(
    ['vacancies', 'hrComrades'], 
    ({vacancies, hrComrades }) => {
      const vacancyName = document.getElementById('hr-tool-position-input').value
      const vacancy = vacancies.find(vacancy => vacancy.name === vacancyName)
      const vacancyPath = vacancy && vacancy.pathName

      const responsibleHrName = document.getElementById('hr-tool-recruiter-input').value
      const responsibleHr = hrComrades.find(hr => hr.name === responsibleHrName)
      const responsibleHrSlackId = responsibleHr && responsibleHr.slackId

      const res = {
        firstname: fio.split(' ')[1] || '',
        lastname: fio.split(' ')[0] || '',
        age: document.getElementById('hr-tool-age-input').value,
        github: document.getElementById('hr-tool-github-input').value,
        comment: document.getElementById('hr-tool-comment-input').value,
        location: document.getElementById('hr-tool-city-input').value,
        email: document.getElementById('hr-tool-email-input').value,
        telegram: document.getElementById('hr-tool-telegram-input').value,
        skype: document.getElementById('hr-tool-skype-input').value,
        phone: document.getElementById('hr-tool-phone-input').value,
        hh: document.getElementById('hr-tool-hh-input').value,
        moiKrug: document.getElementById('hr-tool-mk-input').value,
        linkedIn: document.getElementById('hr-tool-linked-input').value,
        fb: document.getElementById('hr-tool-fb-input').value,
        tags: source,
        vacancy: vacancyPath,
        responsibleHr: responsibleHrSlackId,
      };
    
      const request = new XMLHttpRequest();
      const url = `${HOST}/api/candidates`;
    
      // отображаем состояния запроса
      request.onreadystatechange = () => {
        if (request.readyState === 4) {
          document.getElementById('progress').style.display = 'none';
          if (JSON.parse(request.response).ok) {
            document.getElementById('sended').style.display = 'block';
          } else {
            document.getElementById('failed').style.display = 'block';
            document.body.append(request.response);
          }
        }
      };
    
      // посылаем запрос
      request.open('POST', url, true);
      request.setRequestHeader('Requested-From-Chore-Plugin', 'true');
      request.setRequestHeader('Content-Type', 'application/json');
      request.send(JSON.stringify(res));
    
      document.getElementById('sendBtn').style.display = 'none';
      document.getElementById('progress').style.display = 'block';
  });
}

function saveValueToStorage(input) {
  return function(event) {
    localStorage.setItem(input, event.target.value);
  };
}

function rememberValue(input) {
  return function(event) {
    chrome.tabs.sendMessage(activeTab.id, {
      text: 'remember_value',
      key: input,
      value: event.target.value
    });
  }
}

function checkRecruiter() {
    const recruiterInput = document.getElementById('hr-tool-recruiter-input');

    if (recruiterInput.value) {
        recruiterInput.style.background = 'white';
        document.getElementById('sendBtn').disabled = false;
    } else {
        recruiterInput.style.background = 'pink';
        document.getElementById('sendBtn').disabled = true;
    }
}

function checkDuplicates() {
  const res = {
    fio: document.getElementById('duplicate-name-input').value || '',
    email: document.getElementById('duplicate-email-input').value || '',
    nick: document.getElementById('duplicate-nick-input').value || '',
  };

  if (!res.fio && !res.email && !res.nick) {
    return;
  }

  const searchParams = new URLSearchParams()
  searchParams.append("fio", res.fio)
  searchParams.append("email", res.email)
  searchParams.append("nick", res.nick)

  chrome.storage.sync.get(
    ['vacancies'], 
    ({ vacancies }) => {
      const request = new XMLHttpRequest();
      const url = `${HOST}/api/candidates/duplicates?${searchParams}`;

      request.onreadystatechange = () => {
        if (request.readyState === 4) {
          const duplicates = JSON.parse(request.response);

          document.getElementById('tab1').innerHTML = `Дубли (${duplicates.length})`;

          if (!duplicates.length) {
            document.getElementById('duplicatesTable').style.display = 'none';
            document.getElementById('duplicatesNotFound').style.display = 'block';
          } else {
            document.getElementById('duplicatesNotFound').style.display = 'none';
    
            const table = document.getElementById('duplicatesTable');
            const body = document.getElementById('duplicatesTableBody');
    
            body.innerHTML = '';
            table.style.display = 'table';
    
            for (let i = 0; i < duplicates.length && i < 10; i++) {
              const { fio, city, age, email, pathName, vacancyId } = duplicates[i];
              const vacancy = vacancies.find(vacancy => vacancy.id === vacancyId)

              const tableRow = body.insertRow();
              const fioCell = tableRow.insertCell();
              const cityCell = tableRow.insertCell();
              const ageCell = tableRow.insertCell();
              const positionCell = tableRow.insertCell();
              const emailCell = tableRow.insertCell();

              const linkCell = tableRow.insertCell();
              const link = document.createElement('a');
              const linkHref = `${HOST}/candidates/${vacancy.pathName}/${pathName}`
              link.setAttribute('href', linkHref);
              link.setAttribute('target', '_blank');
              link.innerHTML = 'Ссылка на профиль'
    
              fioCell.innerHTML = (fio || '').replace(' ', '<br />');
              cityCell.innerHTML = city || '';
              ageCell.innerHTML = age || '';
              positionCell.innerHTML = vacancy.name || '';
              emailCell.innerHTML = (email|| '').replace('@', '<br />@');
              linkCell.append(link);
            }
          }
        }
      }

      request.open('GET', url, true);
      request.setRequestHeader('Content-Type', 'application/json');
      request.setRequestHeader('Requested-From-Chrome-Plugin', 'true');
      request.send();
  });
}

const valuesToSave = [
  'position', 
  'recruiter', 
];

const valuesToRemember = [
  'name',
  'city',
  'phone',
  'email',
  'github',
  'skype',
  'fb',
  'hh',
  'mk',
  'telegram',
  'linked',
  'age',
  'source',
  'comment',
  'position',
  'recruiter'
];

function addListeners() {
    document.getElementById('sendBtn').addEventListener('click', sendData);
    document.getElementById('duplicatesButton').addEventListener('click', checkDuplicates);
    document.getElementById('hr-tool-recruiter-input').addEventListener('input', checkRecruiter);
    document.getElementById('tab0').addEventListener('click', showLayout(0));
    document.getElementById('tab1').addEventListener('click', showLayout(1));

    valuesToSave.forEach(value =>
        document.getElementById(`hr-tool-${value}-input`).addEventListener('input', saveValueToStorage(value))
    );

    valuesToRemember.forEach(value =>
        document.getElementById(`hr-tool-${value}-input`).addEventListener('input', rememberValue(value))
    );
}

function saveValues(values = {}) {
    addListeners();

    const {
        fio = '',
        phone = '',
        email = '',
        age = '',
        href = '',
        city = '',
        github = '',
        skype = '',
        FB = '',
        comment = '',
        source = '',
        MK = '',
        HH = '',
        linked = '',
        telegram = '',
    } = values;

    valuesToSave.forEach(value =>
        document.getElementById(`hr-tool-${value}-input`).value = localStorage.getItem(value) || ''
    );

    document.getElementById('duplicate-name-input').value = fio;
    document.getElementById('hr-tool-name-input').value = fio;
    document.getElementById('hr-tool-phone-input').value = phone;
    document.getElementById('duplicate-email-input').value = email;
    document.getElementById('hr-tool-email-input').value = email;
    document.getElementById('hr-tool-age-input').value = age;
    document.getElementById('hr-tool-href-input').value = href;
    document.getElementById('hr-tool-city-input').value = city;
    document.getElementById('hr-tool-skype-input').value = skype;
    document.getElementById('hr-tool-github-input').value = github;
    document.getElementById('hr-tool-fb-input').value = FB;
    document.getElementById('hr-tool-hh-input').value = HH;
    document.getElementById('hr-tool-telegram-input').value = telegram;
    document.getElementById('hr-tool-mk-input').value = MK;ks
    document.getElementById('hr-tool-comment-input').value = comment;
    document.getElementById('hr-tool-linked-input').value = linked;
    document.getElementById('hr-tool-source-input').value = source;

    checkRecruiter();
    checkDuplicates();
}

function showLayout(id) {
  return function() {
    document.getElementById(`tab${id}`).classList.add('active');
    document.getElementById(`tab${(id + 1) % 2}`).classList.remove('active');
    document.getElementById(`layout${id}`).style.display = 'block';
    document.getElementById(`layout${(id + 1) % 2}`).style.display = 'none';
  }
}

const createOptionsForDataList = (dataListId, options, isDataList) => {
    const sourceDataList = document.getElementById(dataListId);

    options.forEach(option => {
        const optionNode = document.createElement('option');

        if (isDataList) {
            optionNode.value = option.name;
        } else {
            optionNode.value = option._id;
            optionNode.innerHTML = option.name;
        }

        sourceDataList.appendChild(optionNode);
    });
}

const getTags = async () => {
    try {
        const tagCategoriesResponse = await fetch(
            `${HOST}/api/settings/tags-categories`
          );

          const tagCategories = await tagCategoriesResponse.json()
          const sourceCategory = tagCategories.find(
            category => category.name === 'Источник'
          );
          const stackCategory = tagCategories.find(
            category => category.name === 'Стек'
          );

          const tagsResponse = await fetch(`${HOST}/api/settings/tags`);
          const tags = await tagsResponse.json();

          const sourceTags = tags.filter(tag => tag.category === sourceCategory._id);
          const stackTags = tags.filter(tag => tag.category === stackCategory._id);

          createOptionsForDataList('hr-tool-source-datalist', sourceTags, true)
          createOptionsForDataList('hr-tool-stack-selectlist', stackTags)
    } catch (error) {
        console.error(error)
    }
}

const getVacancies = async () => {
    try {
        const vacanciesResponse = await fetch(
            `${HOST}/api/public/vacancies/active`
          );
          const vacanciesFull = await vacanciesResponse.json()
          const vacanciesLite = vacanciesFull.map( vacancy => ({ 
            id: vacancy.id,
            pathName: vacancy.pathName,
            name: vacancy.name,
          }))

          createOptionsForDataList('hr-tool-position-list', vacanciesLite, true)

          chrome.storage.sync.set({ vacancies: vacanciesLite });
    } catch (error) {
        console.error(error)
    }
}

const getHrComrades = async () => {
  try {
      const hrComradesResponse = await fetch(
          `${HOST}/api/comrades/hr`
        );

        const hrComrades = await hrComradesResponse.json()

        createOptionsForDataList('hr-tool-recruiter-datalist', hrComrades, true)

        chrome.storage.sync.set({ hrComrades });
  } catch (error) {
      console.error(error)
  }
}


getTags()
getVacancies()
getHrComrades()


// Запрашиваем данные из content-скрипта, у которого есть доступ к DOM'у страницы.
chrome.tabs.query({
  currentWindow: true,
  active: true
}, function (tabs) {
  activeTab = tabs[0];

  chrome.tabs.sendMessage(activeTab.id, { text: 'get_values' }, saveValues);
});