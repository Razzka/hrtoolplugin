// HeadHunter
const values = {};

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (msg.text === 'get_values') {
        const fioSelector = document.querySelector('.resume-header-name');
        const phoneSelector = document.querySelector('[itemprop="telephone"]');
        const emailSelector = document.querySelector('[itemprop="email"]');
        const ageSelector = document.querySelector('[data-qa="resume-personal-age"]');
        const citySelector = document.querySelector('[data-qa="resume-personal-address"]');
        const skypeSelector = document.querySelector('.resume__contacts-personalsite.siteicon.siteicon_skype');
        const githubSelector = document.querySelector('.resume__contacts-personalsite.resume__contacts-personalsite_personal.siteicon');
        const linkedSelector = document.querySelector('.resume__contacts-personalsite.siteicon.siteicon_linkedin > a');
        const mkSelector = document.querySelector('.resume__contacts-personalsite.siteicon.siteicon_moi_krug > a');
        const fbSelector = document.querySelector('.resume__contacts-personalsite.siteicon.siteicon_facebook > a');

        sendResponse({
            fio: values.name || fioSelector && fioSelector.textContent,
            phone: values.phone || phoneSelector && phoneSelector.textContent,
            email: values.email || emailSelector && emailSelector.textContent,
            age: values.age || ageSelector && ageSelector.textContent.split(' ')[0],
            city: values.city || citySelector && citySelector.textContent,
            skype: values.skype || skypeSelector && skypeSelector.textContent,
            source: values.source || 'HH',
            linked: values.linked || linkedSelector && linkedSelector.href,
            HH: values.hh || window.location.href,
            MK: values.mk || mkSelector && mkSelector.href,
            FB: values.fb || fbSelector && fbSelector.href,
            github: values.github || githubSelector && githubSelector.href,
            comment: values.comment || '',
            position: values.position || '',
            invtype: values.invtype || '',
            href: window.location.href
        });
    }

    if (msg.text === 'remember_value') {
        values[msg.key] = msg.value;
    }
});
