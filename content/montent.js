// MoiKrug
const values = {};

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (msg.text === 'get_values') {
        const fioSelector = document.querySelector('.user_name');
        const citySelector = document.querySelector('.geo');
        const githubSelector = document.querySelector('a[href^="https://github.com/"]');
        const fbSelector = document.querySelector('a[href^="https://www.facebook.com/"]:not(.facebook)');
        const telegramSelector = document.querySelector('a[href^="https://telegram.me/"]:not(.telegram)');

        const contacts = document.querySelectorAll('.contacts > .contact');

        let phone = '';
        let email = '';
        let skype = '';
        if (contacts.length) {
            for (let i = 0; i < contacts.length; i++) {
                const text = contacts[i].textContent;

                if (text.indexOf('Телефон:') !== -1) {
                    phone = text.split(': ')[1];
                }

                if (text.indexOf('Почта:') !== -1) {
                    email = text.split(': ')[1];
                }

                if (text.indexOf('Skype:') !== -1) {
                    skype = text.split(': ')[1];
                }
            }
        }

        // age
        const ageAndStage = document.querySelectorAll('.experience_and_age > .row');

        let age = '';
        if (ageAndStage.length) {
            for (let i = 0; i < ageAndStage.length; i++) {
                const text = ageAndStage[i].textContent;

                if (text.indexOf('Возраст') !== -1) {
                    age = text.split(': ')[1];
                }
            }

            if (age) {
                age = age.split(' ')[0]; // отрезаем "лет, год, года"
            }
        }

        sendResponse({
            fio: values.name || fioSelector && fioSelector.textContent && fioSelector.textContent.split(' ').reverse().join(' ') || '',
            phone: values.phone || phone,
            email: values.email || email,
            age: values.age || age,
            city: values.city || citySelector && citySelector.textContent.replace('Россия, ', ''),
            skype: values.skype || skype,
            source: values.source || 'MK',
            MK: values.mk || window.location.href,
            FB: values.fb || fbSelector && fbSelector.href || '',
            HH: values.hh || '',
            linked: values.linked || '',
            telegram: values.telegram || telegramSelector && telegramSelector.text || '',
            github: values.github || githubSelector && githubSelector.href || '',
            comment: values.comment || '',
            position: values.position || '',
            invtype: values.invtype || '',
            href: window.location.href
        });
    }

    if (msg.text === 'remember_value') {
        values[msg.key] = msg.value;
    }
});
