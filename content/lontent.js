// LinkedIn
const values = {};

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (msg.text === 'get_values') {
        const fioSelector = document.querySelector('.inline.t-24.t-black.t-normal.break-words');
        const citySelector = document.querySelector('.t-16.t-black.t-normal.inline-block');
        const emailSelector = document.querySelector('.pv-contact-info__contact-type.ci-email a');
        const contactSelector = document.querySelector('.pv-contact-info__contact-type.ci-ims');
        const phoneSelector = document.querySelector('.pv-contact-info__contact-type.ci-phone .pv-contact-info__ci-container > span');
        const webSiteSelector = document.querySelectorAll('.pv-contact-info__contact-type.ci-websites a');
        const addressSeletor = document.querySelector('.pv-contact-info__contact-type.ci-address a');

        const contact = contactSelector && contactSelector.textContent;
        const fio = fioSelector && (fioSelector.textContent || '').trim();
        const city = citySelector && citySelector.textContent.replace(', Россия', '').replace('Россия', '').trim();
        const address = addressSeletor && addressSeletor.textContent.trim();
        const icq = contact && contact.indexOf('ICQ') !== -1;
        const skype = contact && contact.indexOf('Skype') !== -1;
        let github = '';
        let facebook = '';

        webSiteSelector.length && webSiteSelector.forEach(input => {
            if (input.href.indexOf('facebook') !== -1) {
                facebook = input.href;
            }

            if (input.href.indexOf('github') !== -1) {
                github = input.href;
            }
        });

        sendResponse({
            fio: values.name || fio && fio.split(' ').reverse().join(' '),
            city: values.city || city || address || '',
            phone: values.phone || phoneSelector && phoneSelector.textContent,
            age: values.age || '',
            email: values.email || emailSelector && emailSelector.textContent.trim(),
            skype: values.skype || skype ? contactSelector.querySelector('.pv-contact-info__contact-item').textContent.trim() : '',
            comment: values.comment || icq ? `ICQ: ${contactSelector.querySelector('.pv-contact-info__contact-item').textContent.trim()}` : '',
            FB: values.fb || facebook,
            HH: values.hh || '',
            MK: values.mk || '',
            github: values.github || github,
            source: values.source || 'Linkedin',
            linked: values.linked || window.location.href,
            position: values.position || '',
            invtype: values.invtype || '',
            href: window.location.href
        });
    }

    if (msg.text === 'remember_value') {
        values[msg.key] = msg.value;
    }
});
